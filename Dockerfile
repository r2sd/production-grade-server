FROM tiangolo/meinheld-gunicorn:python3.8-alpine3.11
COPY requirements.txt /app
RUN pip install -r /app/requirements.txt
ENV PYTHONPATH="/app/src"
ENV MODULE_NAME="src.app"
COPY . /app
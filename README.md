# Production grade backend application

1. Based on this [Python template](https://bitbucket.org/r2sd/python-template).
   1. Make sure to thoroughly go through the `README.md` of the template above.
2. Provides highest possible performance with Flask and WSGI.
3. Containerised.
4. Supports Nested Diagnostic Context based logging for thorough log tracing.
5. For development purpose:
   1. Everything starts at `src/app.py`. This is the place to start exploring this codebase.
   2. `cd` into the directory where this file is in.
   3. Add the `./src/` directory to your PYTHONPATH using `export PYTHONPATH="${PYTHONPATH}:$PWD/src"`.
   4. Run the app using `export FLASK_APP=src/app.py && export FLASK_DEBUG=1 && flask run` or `export FLASK_DEBUG=1 && python src/app.py`.
6. For production build:
   1. `pipenv lock -r > requirements.txt && docker build -t localhost:5000/shridharshan/backend:latest .`
   2. Please note that [a local Docker registry is running at localhost:5000](https://docs.docker.com/registry/deploying/#run-a-local-registry) to speed things up.

# Thoroughly understand https://docs.python.org/3/library/logging.html before making changes
from flask import Flask, request
from typing import Optional
import datetime
import logging
import sys
import uuid

from constants import DEFAULT_REQUEST_ID


class NestedDiagnosticContextFormatter(logging.Formatter):
    """
    Nested Diagnostic Context (NDC) is a mechanism to help distinguish interleaved log
    messages from different sources. NDC does this by providing the ability to add
    distinctive contextual information to each log entry.
    For more information, See: https://www.baeldung.com/java-logging-ndc-log4j

    In this case, we use a unique request_id to distinguish log messages from various
    requests.
    """

    def format(self, record: logging.LogRecord):
        record.request_id = self._get_request_id()
        return super().format(record)

    def _get_request_id(self) -> str:
        try:
            return self._get_request_id_from_from_request()
        except Exception:
            return DEFAULT_REQUEST_ID

    def _get_request_id_from_from_request(self) -> str:
        request_id: Optional[str] = getattr(request, "request_id", None)
        if not request_id:
            request.request_id = self._generate_id()
            request_id = request.request_id
        return request_id

    def _generate_id(self) -> str:
        return uuid.uuid4().hex

    def formatTime(self, record: logging.LogRecord, datefmt=None):
        return datetime.datetime.utcnow().strftime(r"%Y-%m-%d %H:%M:%S +0000")


def init_logger(app: Flask):
    logger = logging.getLogger()  # Get `root` logger, to be configured below
    logger.setLevel(logging.DEBUG)  # No log messages should be skipped

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(
        NestedDiagnosticContextFormatter(
            fmt="%(asctime)s [%(request_id)s] [%(name)s] [%(levelname)s] %(message)s"
        )
    )

    logger.addHandler(console_handler)

    log = logging.getLogger("werkzeug")
    log.disabled = True

from flask import Flask, request, Response
from setup import logger_setup
import logging

from constants import (
    DEFAULT_REQUEST_ID,
    REQUEST_END,
    REQUEST_START,
    X_REQUEST_ID,
)
from . import routes
from .routes import shout

app = Flask(__name__)
logger_setup.init_logger(app)

logger = logging.getLogger(__name__)

app.register_blueprint(routes.blueprint, url_prefix="/")
app.register_blueprint(shout.blueprint, url_prefix="/shout")


@app.before_request
def before_request():
    logger.info(
        '%s %s "%s %s %s"',
        REQUEST_START,
        request.remote_addr,
        request.scheme,
        request.method,
        request.path,
    )


@app.after_request
def after_request(response: Response):
    response.headers.setdefault(
        X_REQUEST_ID, getattr(request, "request_id", DEFAULT_REQUEST_ID)
    )
    logger.info("%s %s", REQUEST_END, response.status)
    return response


if __name__ == "__main__":
    app.run()

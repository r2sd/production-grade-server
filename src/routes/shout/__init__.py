from flask import Blueprint
import logging

blueprint = Blueprint("shout", __name__)
logger = logging.getLogger(__name__)


@blueprint.route("/")
def error():
    logger.warn("about to crash")
    raise Exception("shout")
    return "Never gonna print!"

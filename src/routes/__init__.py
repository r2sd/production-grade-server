from flask import Blueprint, request
import logging

blueprint = Blueprint("root", __name__)
logger = logging.getLogger(__name__)


@blueprint.route("/<string:first_name>")
def hello_world(first_name: str):
    second_name = request.args.get("second_name", ", Good morning")
    logger.info(
        "hello from / with first_name: [%s] and second_name: [%s]",
        first_name,
        second_name,
    )
    return f"Hello, {first_name} {second_name}!"
